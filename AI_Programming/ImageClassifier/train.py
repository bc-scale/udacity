'''
From the instructions:
    Train a new network on a data set with train.py
    Basic usage: python train.py data_directory
    Prints out training loss, validation loss, and validation accuracy as the network trains
    Options:
        -Set directory to save checkpoints: python train.py data_dir --save_dir save_directory
        -Choose architecture: python train.py data_dir --network "vgg13"
        -Set hyperparameters: python train.py data_dir --learning_rate 0.01 --model_nodes 512 --epochs
        -Use GPU for training: python train.py data_dir --gpu
'''

# Imports here
import torch, torchvision, json, time, argparse, os

import torch.nn.functional as F
import numpy as np

from torch import nn, optim
from torch.autograd import Variable
from torchvision import datasets, transforms, models
from collections import OrderedDict
# from PIL import Image

#default parameters that make the code cleaner
data_dir_param = ''
save_dir_param = None
model_type_param = 'vgg13'
train_dir_param = 'flowers/train'
valid_dir_param = 'flowers/valid'
test_dir_param = 'flowers/test'

features_in_param = 25088
model_layers_param = 1024
learning_rate_param = 0.0005
batch_size_param = 64
drop_out_param = 0.5
epochs_param = 2
gpu_param = True
save_model_param = 'model_checkpoint.pth'


#define parser input arguments
def parser_arguments():
    parser = argparse.ArgumentParser(description = "Image Classifier Training")
    parser.add_argument('--data_dir', type = str, default = data_dir_param, help = 'Select the path to data')
    parser.add_argument('--save_dir', type = str, default = save_dir_param, help = 'Select path for saving the network')
    parser.add_argument('--model_type', type = str, default = model_type_param, choices = ['vgg11', 'vgg13', 'vgg16', 'vgg19'],
                        help = 'Select the type of model')
    parser.add_argument('--learning_rate', type = float, default = learning_rate_param, help = 'Select the learning rate')
    parser.add_argument('--model_layers', type = int, default = model_layers_param, help = 'Select the number of layers')
    parser.add_argument('--epochs', type = int, default = epochs_param, help = 'Select the number of training epochs')
    parser.add_argument('--gpu', action = "store_true", default = gpu_param, help = 'Select if GPU should be used')

    args = parser.parse_args()
    
    return args.data_dir, args.save_dir, args.model_type, args.learning_rate, args.model_layers, args.epochs, args.gpu

#get the data directories for training, validation and testdata
def load_data_dir(data_dir):
    training_dir = data_dir + train_dir_param
    validation_dir = data_dir  + valid_dir_param
    test_dir = data_dir  + test_dir_param

    return training_dir, validation_dir, test_dir

#get the amount of classes from the training data - this will determine the number of out features the model should have
def load_features_out(training_dir, validation_dir, test_dir):
    n_training = len(os.listdir(training_dir))
    n_validation = len(os.listdir(validation_dir))
    n_test = len(os.listdir(test_dir))

    features_out = n_training
    return features_out

#prepare datasets and transform the images accordingly
def prepare_datasets(training_dir, validation_dir, test_dir):
    transforms_train = transforms.Compose([transforms.RandomRotation(30),
                                                transforms.RandomResizedCrop(224),
                                                transforms.RandomHorizontalFlip(),
                                                transforms.ToTensor(),
                                                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                                                ])
    transforms_valid = transforms.Compose([transforms.Resize(256),
                                                transforms.CenterCrop(224),
                                                transforms.ToTensor(),
                                                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                                                ])
    transforms_test = transforms.Compose([transforms.Resize(256),
                                               transforms.CenterCrop(224),
                                               transforms.ToTensor(),
                                               transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                                               ])
    training_data = datasets.ImageFolder(training_dir, transform = transforms_train)
    validation_data = datasets.ImageFolder(validation_dir, transform = transforms_valid)
    testing_data = datasets.ImageFolder(test_dir, transform = transforms_test)
    
    return training_data, validation_data, testing_data

#define the loader functions to load the images in batches from the directories
def load_data(training_data, validation_data, testing_data):
    training_loader = torch.utils.data.DataLoader(training_data, batch_size = batch_size_param, shuffle = True)
    validation_loader = torch.utils.data.DataLoader(validation_data, batch_size = batch_size_param)
    testing_loader = torch.utils.data.DataLoader(testing_data, batch_size = batch_size_param)

    return training_loader, validation_loader, testing_loader

#load the labels for mapping from the json file
def labels_mapping(data_directory):
    #file path to mapping file                                         
    mapping_file = 'cat_to_name.json'
    #load json
    with open(mapping_file, 'r') as map:
        mapping = json.load(map)
        return mapping

#load the pretrained model
def pretrained_model(model_type, model_layers, features_out):
    model = getattr(torchvision.models, model_type)(pretrained = True)
                                          
    for param in model.parameters():
        param.requires_grad = False
        
    classifier = nn.Sequential(OrderedDict([
                          ('fc1', nn.Linear(features_in_param, model_layers)),
                          ('drop', nn.Dropout(p = drop_out_param)),
                          ('relu', nn.ReLU()),
                          ('fc2', nn.Linear(model_layers, features_out)),
                          ('output', nn.LogSoftmax(dim=1))
                          ]))
    model.classifier = classifier
    
    return model, classifier

#safe the model to be used for prediction in predict.py
def save_model(model, classifier, training_data, model_type, features_out, learning_rate, epochs,
                          optimizer, safe_dir, save_model_param):
    model.class_to_idx = training_data.class_to_idx
    
    save_model = {'network' : model_type,
              'features_in' : features_in_param,
              'features_out' : features_out,
              'learning_rate' : learning_rate,
              'batch_size' : batch_size_param,
              'classifier' : classifier,
              'epochs' : epochs,
              'optimizer' : optimizer.state_dict(),
              'state_dict' : model.state_dict(),
              'class_to_idx' : model.class_to_idx}

    torch.save(save_model, safe_dir)
    
#run the whole training sequence with the pretrained model
def run_training(data_dir, save_dir, model_type, model_layer, learning_rate, epochs, gpu):
    #load directories
    training_dir, validation_dir, test_dir = load_data_dir(data_dir)
    #prepare the data
    training_data, validation_data, testing_data = prepare_datasets(training_dir, validation_dir, test_dir)
    #load the data
    training_loader, validation_loader, testing_loader = load_data(training_data, validation_data, testing_data)
    #get the amount of "features out"
    features_out = load_features_out(training_dir, validation_dir, test_dir)
    #get the label mapping
    category_names = labels_mapping(data_dir)
    #load the pretrained model
    model, classifier = pretrained_model(model_type, model_layers, features_out)

  # Start the actual training of the network
    #check if GPU is there
    if gpu == True:
        # Use GPU if it's available
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    #training model parameters
    criterion = nn.NLLLoss()
    optimizer = optim.Adam(model.classifier.parameters(), lr = learning_rate)
    model.to(device)
    #set evaluation = True to evaluate the model against validation data
    evaluation = True
    
    print('Start training the network')
    
    #loop through the epochs for training and evaluation
    for epoch in range(epochs):
        training_loss = 0
        for inputs, labels in training_loader:     
            inputs, labels = inputs.to(device), labels.to(device)
            
            #reset optimizers
            optimizer.zero_grad()
            
            log_ps = model.forward(inputs)
            loss = criterion(log_ps, labels)
            loss.backward()
            optimizer.step()

            training_loss = training_loss + loss.item()
            
        #print results of training epochs and training Loss per epoch
        print('\n Epoch: {}/{} '.format(epoch + 1, epochs), '\n Training-Step Loss: {:.4f}  '.format(training_loss / len(training_loader)))
         
        #evaluation step if set to TRUE   
        if evaluation == True:
            #set validation baseline
            evaluation_loss = 0
            evaluation_accuracy = 0
            #put model into evaluation mode
            model.eval()

            with torch.no_grad():
                for inputs, labels in validation_loader:
                    inputs, labels = inputs.to(device), labels.to(device)
                    
                    log_ps = model.forward(inputs)
                    loss = criterion(log_ps, labels)
            
                    evaluation_loss = evaluation_loss + loss.item()
            
                    # Calculate accuracy
                    ps = torch.exp(log_ps)
                    top_ps, top_class = ps.topk(1, dim = 1)
                    
                    equals = top_class == labels.view(*top_class.shape)
                    
                    evaluation_accuracy = evaluation_accuracy + torch.mean(equals.type(torch.FloatTensor)).item()
            
            model.train()
            #print results of evaluation
            print('\n Evaluation of training step:\n Loss: {:.4f}  '.format(evaluation_loss / len(validation_loader)),
                  'Accuracy: {:.4f}'.format(evaluation_accuracy / len(validation_loader)))
            
    #Run the testing-Loop of the network against testdata
    print('\n Testing with against testdata:')
    test_loss = 0
    test_accuracy = 0
    model.eval()

    for inputs, labels in testing_loader:
        inputs, labels = inputs.to(device), labels.to(device)

        log_ps = model.forward(inputs)
        loss = criterion(log_ps, labels)
        
        test_loss = test_loss + loss.item()

        # Calculate accuracy
        ps = torch.exp(log_ps)
        top_ps, top_class = ps.topk(1, dim = 1)

        equals = top_class == labels.view(*top_class.shape)

        test_accuracy = test_accuracy + torch.mean(equals.type(torch.FloatTensor)).item()
    
    #print results of testing
    print('\n Losses: {:.4f}  '.format(test_loss / len(testing_loader)),
          'Accuracy: {:.4f}'.format(test_accuracy / len(testing_loader)))

  # Save the model as loadable model_checkpoint
    save_path = ''
    if save_dir == None:
        save_path = save_model_param
    else:
        save_path = save_dir + '/' + save_model_param
    
    save_model(model, classifier, training_data, model_type, features_out, learning_rate, epochs,
                          optimizer, save_path, save_model_param)                 
    print('\n Save the checkpoint in {}'.format(save_path))      


#start main function
if __name__ == "__main__":
    data_dir, save_dir, model_type, learning_rate, model_layers, epochs, gpu = parser_arguments()

    run_training(data_dir, save_dir, model_type, model_layers, learning_rate, epochs, gpu)
 #end