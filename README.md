
# Hello World
This is my personal respository. Feel free to browse around. 
Everything licensed under [License](LICENSE)

> Contact
> - [Linkedin](https://www.linkedin.com/in/hartmuthgieldanowski/)
> - [Twitter](http://twitter.com/hgieldanowski)


## Why
I decided to use my time during the Corona Lockdown to enhance my knowledge on AI and Programming - so I decided to the Udacity AI Programming with Python training. 

## Udacity
Rebooting 2020 - CoronaStlye.

# Next Steps
- Starting with [AI Programming with Python](AI_Programming)

continuing with
- iOS Dev
- Autonomous Track
- etc.


# How I work
![Core](/readme_files/core.jpeg)